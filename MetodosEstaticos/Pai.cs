﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosEstaticos
{
    class Pai
    {
        public static string nomeStatic;
        public string nome;

        public static void metodoEstatico()
        {
            Console.WriteLine("Meotodo Estatico");
        }

        public void metodoNaoEstatico()
        {
            Console.WriteLine("Meotodo Nao Estatico");
        }
    }
}
