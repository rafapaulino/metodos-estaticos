﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosEstaticos
{
    class Program
    {
        static void Main(string[] args)
        {
            Pai.nomeStatic = "Teste";
            Pai.metodoEstatico();

            Pai pai = new Pai();
            pai.metodoNaoEstatico();
            pai.nome = "Nome";

            Console.ReadKey();           
        }
    }
}
